import pyttsx3 
import speech_recognition as sr 
import datetime
from time import ctime
import wikipedia 
import os
import smtplib #for jarvis mail
from PyDictionary import PyDictionary
from playsound import playsound

botMail = '' # Put your burner mail here, after turning on 
botMailPassword = 

dictionary = PyDictionary() #dictionary 

to = ' ' #email address to send email to

engine = pyttsx3.init()
voices = engine.getProperty('voices')

#print(voices[1].id)
engine.setProperty('voice', voices[0].id)
engine.setProperty('rate', 230)

def say(audio):
    engine.say(audio)
    engine.runAndWait()
    
    
def exit_alert():
    say("Due to the pandemic of Corona Virus, i insist you to not use me, and instead focus on your studies. Exiting....")
    print("Due to the pandemic of Corona Virus, i insist you to not use me, and instead focus on your studies. Exiting....")
    exit()

def hello():
    print("Stand By for retinal and biometric scan... \nScan Accepted. Hello Pranav, I am Edith, Tony Stark's Augmented reality security and defence system.")
    #say("Please Stand By for retinal and biometric scan... Scan Accepted. Hello Pranav, I am Edith, Tony Stark's Augmented reality, security, and defence system. What would you like me to do?")        
    
    
def takeCommand():
    #It takes microphone input from the user and returns a string output hehe

    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("...")
        r.pause_threshold = 0.8
        audio = r.listen(source)

    try:
        print("***")    
        query = r.recognize_google(audio, language='en-in')
        

    except Exception :
        print("---")  
        return "None"
    return query



def sendEmail(to, content):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(botMail, botMailPassword)
    server.sendmail(botMail, to, content)
    server.close()



def respond(query):

    try:
        #print("You said: " + query)
        

        if 'time' in query:
            playsound(doneListening)
            now = datetime.datetime.now()
            hours = (now.strftime("%H"))
            minutes = (now.strftime("%M"))
            seconds = (now.strftime("%S"))
            printTime = ("it's " + hours + " hours, " + minutes + " minutes, and " + seconds + " seconds")
            print(printTime)
            say(printTime)

        elif 'send an email' in query:
            playsound(doneListening)
            try:
                print('whom would you like to send?')
                say("whom would you like to send?")
                playsound(doneListening)
                
                recipientName = str.lower(takeCommand())
                
                if recipientName == "ME":
                    to = "MY EMAIL"

                elif recipientName == "PERSON 2":
                    to = "THEIR EMAIL"        
            
                    
      
                
                print('What would you like to send?')
                say('What would you like to send?')
                playsound(doneListening)
                message = takeCommand()
                content = str(message + " P.S. This mail was send by Edith, requested by User Pranav.")  

                sendEmail(to, content)
                emailSentResponse = "Jarvis will send your email shortly."
                playsound(doneListening)
                print(emailSentResponse)
                say(emailSentResponse)
                

            except Exception as e:
                playsound(doneListening)
                print(e)
                say("Sorry Sir, email is currently facing some issues. Please wait for a while and try again later. Anything else you want me to do?")    

        

        elif 'who are you' in query: 
            playsound(doneListening)
            print("oh hey i am Tony Stark's Edith, and i am always here in these glasses.")   
            say("oh hey i am Tony Stark's Edith, and i am always here in these glasses.")    
            
        elif 'what is my name' in query:
            playsound(doneListening)
            print('your name is pranow. and i hope you dont forget it again... haahaa')
            say('your name is pranav. and i hope you dont forget it again... haahaa')

       
        elif 'schedule' in query:
            _day = datetime.datetime.now()
            currentDay = _day.strftime("%A")
            scheduleStr = str("Pranav, it's " + currentDay + " today. So... not much!")
            playsound(doneListening)
            print(scheduleStr)
            say(scheduleStr)

        elif 'speak' in query:          
            playsound(doneListening)
            print("Alright. Connecting to Jarvis Server.")
            say("Alright. Connecting to Jarvis Server.")
            engine.setProperty('voice', voices[0].id)
            print("Hello Sir, Jarvis speaking. how are you?")
            say("Hello Sir, Jarvis speaking. how are you?")
        
        elif "college" in query:
            playsound(doneListening)
            print("College. is that a Target? . Do you want to initiate an attack right now?")
            say("College. is that a Target? . Do you want to initiate an attack right now?")
            say('...')
            say('...')
            say('Cancelling in...')
            say('...')
            say('...')
            say('...3')
            say('...')
            say('...')
            say('...2')
            say('...')
            say('...')
            say('...1')
            say('...')
            say('...')
            print('Okay. attack cancelled.')
            say('Okay. attack cancelled.')
       
        elif 'happy' in query or 'new' in query or 'year' in query:
            say("Mandalorian is disappointed!!!")


       
        elif ('bye' or 'shut down' or 'shutdown') in query: 
            playsound(doneListening)
            print("Turning off!")
            say("Turning off!")       
            exit()

        elif query == "hey jarvis" :
            engine.setProperty('voice', voices[0].id)
            say("here for your help. tell me what can i do")

    except:
        playsound(doneListening)
        say("i am sorry i didnt get what you said.")


if __name__ == "__main__":
   
    hello()
    playsound(doneListening)
   
    while True:
    # if 1:
     playsound(doneListening)
     query = takeCommand().lower()
     
     if 'hey' in query:
        print("YOU SAID: " + query)
        playsound(doneListening)
        respond(query)
     else:
        print("Start your sentence with 'Hey', for me to answer! ")    
